FROM joelmuehlena/librdkafkago-alpine AS builder

WORKDIR /go-app
COPY . .

RUN go env -w GOPROXY='https://gitlab.com/api/v4/projects/25391698/packages/go,https://proxy.golang.org,direct'
RUN go env -w GONOSUMDB="gitlab.com/joel-muehlena-website/utility/jm-config-server-addon-go,sum.golang.org"

ARG GITLABEMAIL
ARG GITLABTOKEN
RUN echo machine gitlab.com login $GITLABEMAIL password $GITLABTOKEN  >> ~/.netrc

RUN go mod download
RUN go build -tags musl -o main-build

FROM alpine

ENV GOOS=linux

COPY --from=builder /usr/lib/pkgconfig /usr/lib/pkgconfig
COPY --from=builder /usr/lib/librdkafka* /usr/lib/

WORKDIR /dist

COPY  --from=builder /go-app/main-build ./api-gateway/main
# COPY config/jwtAccessToken.key.pub ./api-gateway/config/jwtAccessToken.key.pub
# COPY config/jwtAccessToken.key.pub ./config/jwtAccessToken.key.pub

EXPOSE 80

CMD ["./api-gateway/main"]
