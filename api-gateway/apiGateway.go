package api_gateway

import (
	"path/filepath"
	"strconv"

	"gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/config"
	"gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/types"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type ApiGateway struct {
	EndpointsConfig   *[]types.EndpointsConfig
	ApplicationConfig *types.ApplicationConfig
	Router            *gin.Engine
}

func New() (apiGateway *ApiGateway) {
	//Create a new gin router
	router := gin.Default()

	configFilePath := "config/config.yaml"

	endpointsConfig := make([]types.EndpointsConfig, 0)
	var applicationConfig types.ApplicationConfig
	configFileAbsPath, _ := filepath.Abs(configFilePath)
	config.ApplicationConfigParser(configFileAbsPath, &applicationConfig)

	for _, endpoint := range applicationConfig.Application.Endpoints {
		endpointFileAbsPath, _ := filepath.Abs(endpoint.Path)
		//parse config yaml into go
		var configStruct types.EndpointsConfig
		config.RouteConfigParser(endpointFileAbsPath, &configStruct)
		endpointsConfig = append(endpointsConfig, configStruct)
	}

	apiGateway = &ApiGateway{
		&endpointsConfig,
		&applicationConfig,
		router,
	}

	return
}

func NewWithConfigFile(configFilePath string) (apiGateway *ApiGateway) {
	//Create a new gin router
	router := gin.Default()

	if configFilePath == "" {
		configFilePath = "config/config.yaml"
	}

	endpointsConfig := make([]types.EndpointsConfig, 0)
	var applicationConfig types.ApplicationConfig
	configFileAbsPath, _ := filepath.Abs(configFilePath)
	config.ApplicationConfigParser(configFileAbsPath, &applicationConfig)

	for _, endpoint := range applicationConfig.Application.Endpoints {
		endpointFileAbsPath, _ := filepath.Abs(endpoint.Path)
		//parse config yaml into go
		var configStruct types.EndpointsConfig
		config.RouteConfigParser(endpointFileAbsPath, &configStruct)
		endpointsConfig = append(endpointsConfig, configStruct)
	}

	apiGateway = &ApiGateway{
		&endpointsConfig,
		&applicationConfig,
		router,
	}

	return
}

func (apiGateway *ApiGateway) configureEndpointRoutes() {
	versionSet := make(map[string]bool)

	//find all possible versions of the api
	for _, endpoint := range *apiGateway.EndpointsConfig {
		for _, route := range endpoint.Data {
			filterVersionsFromRoute(route, &versionSet)
		}
	}

	//create handler for each route, each version and each method
	for versionPath := range versionSet {
		//new gin Group for a version in the set
		group := apiGateway.Router.Group(versionPath)

		for _, endpoint := range *apiGateway.EndpointsConfig {
			//go through all routes
			for _, route := range endpoint.Data {

				var path string
				if endpoint.Scope != "" {
					//the name of the route
					path = "/" + endpoint.Scope + "/" + route.Name + "/*other"
				} else {
					//the name of the route
					path = "/" + route.Name + "/*other"
				}

				serverUrl := route.Server

				//go through all versions
				for _, version := range route.Versions {

					//if the version mathes the key in the set create the handler
					if version == versionPath {
						for _, method := range route.Methods {
							preFilters := method.PreFilters
							postFilters := method.PostFilters

							//create the handler and find the correct controller function
							group.Handle(method.Type, path, func(ctx *gin.Context) {
								apiGateway.MakeRequest(ctx, serverUrl, preFilters, postFilters)
							})
						}
					}
				}
			}
		}
	}
}

func (apiGateway *ApiGateway) SetupHealthRoutes() {
	apiGateway.Router.GET("/", func(c *gin.Context) {
		c.Status(200)
	})
}

func (apiGateway *ApiGateway) SetupCORSMiddleware() {
	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"https://joel.muehlena.de", "https://blog.joel.muehlena.de", "https://admin.joel.muehlena.de", "https://login.joel.muehlena.de"}
	config.AddAllowHeaders("Content-Type", " Content-Length", " Accept-Encoding", " X-CSRF-Token", " Authorization", " accept", " origin", " Cache-Control", " X-Requested-With", "Cookie")
	config.AddExposeHeaders("Content-Type", " Content-Length")
	config.AllowCredentials = true
	config.AddAllowMethods("PUT", "PATCH", "DELETE", "GET", "POST")
	config.AllowWildcard = true

	apiGateway.Router.Use(cors.New(config))
}

func (apiGateway *ApiGateway) Run() {
	apiGateway.SetupCORSMiddleware()
	apiGateway.SetupHealthRoutes()
	apiGateway.configureEndpointRoutes()
	apiGateway.Router.Run(":" + strconv.Itoa(apiGateway.ApplicationConfig.Application.Port))
}

func filterVersionsFromRoute(route types.Route, versionSet *map[string]bool) {
	for _, v := range route.Versions {
		if !(*versionSet)[v] {
			(*versionSet)[v] = true
		}
	}
}
