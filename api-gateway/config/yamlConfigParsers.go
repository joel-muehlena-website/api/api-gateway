package config

import (
	"io/ioutil"

	"gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/types"

	"gopkg.in/yaml.v2"
)

func RouteConfigParser(fileName string, endpointsConfig *types.EndpointsConfig) {
	configFile, err := ioutil.ReadFile(fileName)

	if err != nil {
		panic(err)
	}

	err = yaml.Unmarshal(configFile, endpointsConfig)

	if err != nil {
		panic(err)
	}

}

func ApplicationConfigParser(fileName string, applicationConfig *types.ApplicationConfig) {
	configFile, err := ioutil.ReadFile(fileName)

	if err != nil {
		panic(err)
	}

	err = yaml.Unmarshal(configFile, applicationConfig)

	if err != nil {
		panic(err)
	}
}

func KafkaConfigParser(fileName string, kafkaConfig *types.KafkaConfig) {
	configFile, err := ioutil.ReadFile(fileName)

	if err != nil {
		panic(err)
	}

	err = yaml.Unmarshal(configFile, kafkaConfig)

	if err != nil {
		panic(err)
	}
}
