package filters

import (
	"net/http"
	"reflect"

	"gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/types"

	"github.com/gin-gonic/gin"
)

func ApplyPreFilters(filterChain types.FilterChain, ctx *gin.Context, req *http.Request, isFin chan bool) {

	filterStore := GetPreFilterStore()

	for _, filter := range filterChain {

		if ctx.IsAborted() {
			break
		}

		//Check if filters exist in the store if not skip
		if _, ok := (*filterStore)[filter.Name]; !ok {
			print("[PRE-FILTER-ERROR] The filter ", filter.Name, " does not exist\n")
			continue
		}

		//Call filter function
		f := reflect.ValueOf((*filterStore)[filter.Name])

		in := make([]reflect.Value, 3)
		in[0] = reflect.ValueOf(ctx)
		in[1] = reflect.ValueOf(req)

		if filter.Args == nil {
			in[2] = reflect.ValueOf(make([]interface{}, 0))
		} else {
			in[2] = reflect.ValueOf(filter.Args)
		}

		f.Call(in)
	}

	isFin <- true
}

func ApplyPostFilters(filterChain types.FilterChain, ctx *gin.Context, response *http.Response, isFin chan bool) {

	filterStore := GetPostFilterStore()

	for _, filter := range filterChain {

		if ctx.IsAborted() {
			break
		}

		//Check if filters exist in the store if not skip
		if _, ok := (*filterStore)[filter.Name]; !ok {
			print("[POST-FILTER-ERROR] The filter ", filter.Name, " does not exist\n")
			continue
		}

		//Call filter function
		f := reflect.ValueOf((*filterStore)[filter.Name])

		in := make([]reflect.Value, 3)
		in[0] = reflect.ValueOf(ctx)
		in[1] = reflect.ValueOf(response)

		if filter.Args == nil {
			in[2] = reflect.ValueOf(make([]interface{}, 0))
		} else {
			in[2] = reflect.ValueOf(filter.Args)
		}

		f.Call(in)
	}
	isFin <- true
}
