package filters

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type preFilterStorageType map[string]func(ctx *gin.Context, req *http.Request, args []interface{})
type postFilterStorageType map[string]func(ctx *gin.Context, response *http.Response, args []interface{})

var preFilterStorage preFilterStorageType = map[string]func(ctx *gin.Context, req *http.Request, args []interface{}){}
var postFilterStorage postFilterStorageType = map[string]func(ctx *gin.Context, response *http.Response, args []interface{}){}

func GetPreFilterStore() *preFilterStorageType {
	return &preFilterStorage
}

func GetPostFilterStore() *postFilterStorageType {
	return &postFilterStorage
}

func AddPreFilter(name string, function func(ctx *gin.Context, req *http.Request, args []interface{})) {

	if _, ok := preFilterStorage[name]; ok {
		println("[ADD-PRE-FILTER-ERROR] The filter with the name", name, "already exists")
		return
	}

	preFilterStorage[name] = function
}

func AddPostFilter(name string, function func(ctx *gin.Context, req *http.Response, args []interface{})) {

	if _, ok := postFilterStorage[name]; ok {
		println("[ADD-POST-FILTER-ERROR] The filter with the name", name, "already exists")
		return
	}

	postFilterStorage[name] = function
}
