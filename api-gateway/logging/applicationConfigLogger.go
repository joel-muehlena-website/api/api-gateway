package logging

import (
	"fmt"

	"gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/types"
)

func ApplicationConfigLogger(applicationConfig *types.ApplicationConfig) {

	fmt.Println("----------------------------------")
	fmt.Println("\t\tApplication Config Logger")
	fmt.Println("----------------------------------")

	println("Application name:", applicationConfig.Application.Name)
	println("Port:", applicationConfig.Application.Port)
	println("Endpoints files:")

	for _, endpoint := range applicationConfig.Application.Endpoints {
		println("\t-", endpoint.Name+" : "+endpoint.Path)
	}

	fmt.Println()
}
