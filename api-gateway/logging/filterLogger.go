package logging

import "gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/types"

func LogFilterChain(filterChain types.FilterChain, name string) {

	if len(filterChain) <= 0 {
		println("[Filter-Logger]", name, "FilterChain is empty")
		return
	}

	print("[Filter-Logger] ", name, "FilterChain logger: ")

	for i, filter := range filterChain {
		print(filter.Name)
		if i < len(filterChain)-1 {
			print(", ")
		}
	}

	println()
}
