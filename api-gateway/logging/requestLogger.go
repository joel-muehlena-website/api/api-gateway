package logging

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/config"
	"gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/types"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type ApplicationRequestLogger struct {
	Context        *gin.Context
	ControllerName string
	Scope          string
	Uuid           uuid.UUID
	Error          error
	KafkaEnable    bool
	KafkaPort      int
	KafkaServer    string
	KafkaConfig    *types.KafkaConfig
}

type JSONLog struct {
	TimeStamp           time.Time `json:"timestamp"`
	RequestID           string    `json:"requestId"`
	ClientIP            string    `json:"clientIP"`
	RequestedController string    `json:"requestedController"`
	Finished            bool      `json:"isReqFinished"`
	Error               string    `json:"error"`
}

func (arl *ApplicationRequestLogger) RequestLogger() {
	print("\n[Request Logger] New request to ", arl.ControllerName, " with uuid: ", arl.Uuid.String())

	if arl.Scope != "" {
		print(" Request belongs to Scope: ", arl.Scope)
	}

	print("\n")

	// if arl.KafkaEnable {

	// 	var kafkaConfig types.KafkaConfig
	// 	config.KafkaConfigParser("config/config.yaml", &kafkaConfig)
	// 	arl.KafkaConfig = &kafkaConfig

	// 	logData := JSONLog{
	// 		RequestID:           arl.Uuid.String(),
	// 		ClientIP:            arl.Context.ClientIP(),
	// 		RequestedController: arl.ControllerName,
	// 		Finished:            false,
	// 		TimeStamp:           time.Now(),
	// 	}

	// 	go arl.LogToKafka(logData, "request-logging")
	// }
}

func (arl *ApplicationRequestLogger) FinishRequestLogging() {
	print("\n[Request Logger] Request to ", arl.ControllerName, " with uuid: ", arl.Uuid.String(), " finished\n")

	if arl.Error != nil {
		println("[Request Logger ERROR]", arl.Error.Error())
	}

	// if arl.KafkaEnable {

	// 	logData := JSONLog{
	// 		RequestID:           arl.Uuid.String(),
	// 		RequestedController: arl.ControllerName,
	// 		Finished:            true,
	// 		TimeStamp:           time.Now(),
	// 	}

	// 	if arl.Error != nil {
	// 		logData.Error = arl.Error.Error()
	// 	}

	// 	go arl.LogToKafka(logData, "request-logging")
	// }
}

func (arl *ApplicationRequestLogger) LogToKafka(logData JSONLog, topic string) {

	if arl.KafkaConfig == nil {
		var kafkaConfig types.KafkaConfig
		config.KafkaConfigParser("config/config.yaml", &kafkaConfig)
		arl.KafkaConfig = &kafkaConfig
	}

	var servers string
	for _, topics := range arl.KafkaConfig.Kafka.Setup {
		if topics.Topic == topic {
			servers = strings.Join(topics.Server, ",")
		}
	}

	if servers == "" {
		return
	}

	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": servers})

	if err != nil {
		panic(err)
	}

	defer p.Close()

	go func() {
		for e := range p.Events() {
			switch ev := e.(type) {
			case *kafka.Message:
				if ev.TopicPartition.Error != nil {
					fmt.Printf("[Kafka Log] Delivery failed: %v\n", ev.TopicPartition)
				}
			}
		}
	}()

	logDataJsonString, err := json.Marshal(&logData)

	if err != nil {
		println(err.Error())
	}

	p.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Key:            []byte(arl.Uuid.String()),
		Value:          logDataJsonString,
	}, nil)

	p.Flush(1 * 1000)
}
