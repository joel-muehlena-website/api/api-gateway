package logging

import (
	"fmt"

	"gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/types"
)

func RouteConfigLogger(routeConfig *[]types.EndpointsConfig) {

	fmt.Println("----------------------------------")
	fmt.Println("\t\tRoute Config Logger")
	fmt.Println("----------------------------------")

	for _, endpoint := range *routeConfig {
		fmt.Println("Version of yaml:", endpoint.Version)

		if endpoint.Scope != "" {
			fmt.Println("Scope:", endpoint.Scope)
		}

		for _, route := range endpoint.Data {
			fmt.Println("Route:", route.Name, ":")
			fmt.Println("Server:", route.Server)

			//Print all version of the given route
			fmt.Print("\tVersions: ")
			for i, version := range route.Versions {
				fmt.Print(version)
				if i < len(route.Versions)-1 {
					fmt.Print(", ")
				}
			}
			fmt.Println()

			//Print all methods of the given route
			fmt.Print("\tMethods: ")
			for i, method := range route.Methods {
				fmt.Print(method.Type)
				if i < len(route.Methods)-1 {
					fmt.Print(", ")
				}
			}
			fmt.Println()
		}
		fmt.Println()
	}
}
