package api_gateway

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/filters"
	"gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/logging"
	"gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/types"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func (apiGateway *ApiGateway) MakeRequest(ctx *gin.Context, serverUrl string, preFilters types.FilterChain, postFilters types.FilterChain) {

	//The name of the matching controller

	path := strings.Split(ctx.FullPath(), "/")
	scopeFiltered := path[2]
	scope := ""
	for _, endpoint := range *apiGateway.EndpointsConfig {
		if endpoint.Scope == scopeFiltered {
			scope = scopeFiltered
		}
	}

	var controllerName string
	if scope != "" {
		controllerName = path[1] + ":" + path[3] + ":" + ctx.Request.Method
	} else {
		controllerName = path[1] + ":" + path[2] + ":" + ctx.Request.Method
	}

	//Create uuid for reuqest logging
	requestId := uuid.New()

	//Log Filters to console
	logging.LogFilterChain(preFilters, "preFilter")
	logging.LogFilterChain(postFilters, "postFilter")

	//Log request
	applicationLogger := logging.ApplicationRequestLogger{
		Context:        ctx,
		Scope:          scope,
		ControllerName: controllerName,
		Uuid:           requestId,
		KafkaEnable:    true,
	}
	applicationLogger.RequestLogger()

	//Apply pre filter to Request

	client := &http.Client{}

	req, err := http.NewRequest(ctx.Request.Method, serverUrl+ctx.Request.URL.Path, nil)

	contextBody, _ := ioutil.ReadAll(ctx.Request.Body)

	if ctx.Request.Header.Get("Content-Type") != "" {
		req.Header.Add("Content-Type", ctx.Request.Header.Get("Content-Type"))
		newbody := bytes.NewReader(contextBody)
		req.Body = ioutil.NopCloser(newbody)
	}

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"code": http.StatusInternalServerError,
			"msg":  "Could not create http request",
			"data": err,
		})
		applicationLogger.Error = err
		applicationLogger.FinishRequestLogging()
		return
	}

	isFilterFin := make(chan bool)

	if scope != "" {
		req.Header.Add("x-api-scope", scope)
	}

	go filters.ApplyPreFilters(preFilters, ctx, req, isFilterFin)

	if <-isFilterFin {
		//Add uuid as header to log this request through all services
		req.Header.Add("x-request-uuid", requestId.String())

		if !ctx.IsAborted() {
			response, err := client.Do(req)

			if err != nil {
				ctx.JSON(http.StatusInternalServerError, gin.H{
					"code": http.StatusInternalServerError,
					"msg":  "Failed to make an http request to requested endpoint",
					"data": err,
				})
				applicationLogger.Error = err
				applicationLogger.FinishRequestLogging()
				return
			}

			defer response.Body.Close()

			bodyBytes, err := ioutil.ReadAll(response.Body)
			if err != nil {
				log.Fatal(err)
			}

			go filters.ApplyPostFilters(postFilters, ctx, response, isFilterFin)

			if <-isFilterFin {
				if !ctx.Writer.Written() {
					ctx.Data(response.StatusCode, response.Header.Get("Content-Type"), bodyBytes)
				}

				applicationLogger.FinishRequestLogging()
				return
			}
		} else {
			if !ctx.Writer.Written() {
				ctx.JSON(http.StatusInternalServerError, gin.H{
					"code": http.StatusInternalServerError,
					"msg":  "The request was aborted",
				})
			}
			applicationLogger.FinishRequestLogging()
			return
		}

		//Apply post request filters

	}
}
