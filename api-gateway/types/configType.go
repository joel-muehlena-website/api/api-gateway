package types

type ApplicationConfig struct {
	Application struct {
		Name      string `yaml:"name"`
		Port      int    `yaml:"port"`
		Endpoints []struct {
			Name string `yaml:"name"`
			Path string `yaml:"path"`
		} `yaml:"endpoints"`
	} `yaml:"application"`
}

type KafkaConfig struct {
	Kafka struct {
		Enabled bool `yaml:"enabled"`
		Setup   []struct {
			Topic  string   `yaml:"topic"`
			Server []string `yaml:"server"`
		}
	}
}
