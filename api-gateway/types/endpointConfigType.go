package types

type EndpointsConfig struct {
	Kind    string  `yaml:"kind"`
	Version int32   `yaml:"version"`
	Scope   string  `yaml:"scope"`
	Data    []Route `yaml:"data"`
}

type Route struct {
	Name     string   `yaml:"name"`
	Versions []string `yaml:"versions"`
	Methods  []Method `yaml:"methods"`
	Server   string   `yaml:"server"`
}

type Method struct {
	Type        string      `yaml:"type"`
	PreFilters  FilterChain `yaml:"pre-filters"`
	PostFilters FilterChain `yaml:"post-filters"`
}
