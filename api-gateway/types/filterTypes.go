package types

type FilterChain []Filter

type Filter struct {
	Name string        `yaml:"name"`
	Args []interface{} `yaml:"args"`
}
