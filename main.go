package main

import (
	"os"

	api_gateway "gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway"
	api_gateway_config "gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/config"
	"gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/filters"
	"gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/logging"
	api_gateway_types "gitlab.com/joel-muehlena-website/api/api-gateway/api-gateway/types"
	"gitlab.com/joel-muehlena-website/api/api-gateway/userFilters"
	configServerAddon "gitlab.com/joel-muehlena-website/utility/jm-config-server-addon-go"
)

func main() {

	println("[LOG] Starting server trying to pull config")

	configServerFileEnv := os.Getenv("CONFIG_SERVER_ENV")
	configServerHost := os.Getenv("CONFIG_SERVER_HOST")
	configServerPort := os.Getenv("CONFIG_SERVER_PORT")

	if configServerHost == "" {
		configServerHost = "172.25.186.144"
	}

	if configServerPort == "" {
		configServerPort = "8081"
	}

	if configServerFileEnv == "" {
		configServerFileEnv = "dev"
	}

	configServerUrl := "http://" + configServerHost + ":" + configServerPort + "/config?gitlabFileEnding=yaml&filename="
	filenname := "api_gateway.config:" + configServerFileEnv

	ch := make(chan bool)

	path, _ := os.Getwd()
	path += "/config"

	println("[STARTUP LOG] Current config path: " + path)

	go configServerAddon.FetchConfigServer(configServerUrl+filenname, path, "config", ch)

	state := <-ch

	if !state {
		panic("Cannot pull config")
	} else {
		println("[LOG] Config pull successful")
	}

	close(ch)

	pullEndpoints(configServerFileEnv, configServerUrl, path)

	startServer()
}

func pullEndpoints(configServerFileEnv string, configServerUrl string, path string) {

	var config api_gateway_types.ApplicationConfig
	api_gateway_config.ApplicationConfigParser(path+"/config.yaml", &config)

	ch := make(chan bool, len(config.Application.Endpoints))
	results := make([]bool, len(config.Application.Endpoints))

	for i, endpoint := range config.Application.Endpoints {

		endpointFileName := endpoint.Name + ".endpoints"

		filenname := "api_gateway." + endpointFileName + ":" + configServerFileEnv
		go configServerAddon.FetchConfigServer(configServerUrl+filenname, path, endpointFileName, ch)

		results[i] = <-ch

		if results[i] {
			println("[LOG] Endpoints pull successfull for ", endpoint.Name, " to ", endpoint.Path)
		} else {
			panic("Cannot pull endpoints")
		}
	}
}

func startServer() {
	apiGateway := api_gateway.New()

	logging.RouteConfigLogger(apiGateway.EndpointsConfig)
	logging.ApplicationConfigLogger(apiGateway.ApplicationConfig)

	//Add filter functions
	filters.AddPreFilter("authJWTFilter", userFilters.AuthJWTFilter)
	filters.AddPreFilter("roleFilter", userFilters.RoleFilter)

	apiGateway.Run()
}
