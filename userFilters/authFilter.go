package userFilters

import (
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gopkg.in/yaml.v2"
)

type jwtTokenClaims struct {
	Type      string
	Id        string
	Roles     string
	Username  string
	FirstName string
	LastName  string
	Avatar    string
	jwt.StandardClaims
}

type AuthServerConfig struct {
	Auth struct {
		Server         string `yaml:"server"`
		RefreshPath    string `yaml:"refresh-path"`
		RoleVerifyPath string `yaml:"role-verify-path"`
	}
}

func AuthJWTFilter(ctx *gin.Context, req *http.Request, args []interface{}) {

	type authServerResponse struct {
		Code  int    `json:"status"`
		Token string `json:"token"`
	}

	var authServerConfig AuthServerConfig
	authConfigParser("config/config.yaml", &authServerConfig)

	pk, err := ioutil.ReadFile("config/keys/jwtAccessToken.key.pub")

	if err != nil {
		log.Fatal(err)
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"code": http.StatusInternalServerError,
			"msg":  err.Error(),
		})
		ctx.Abort()
		return
	}

	pkParsed, err := jwt.ParseRSAPublicKeyFromPEM(pk)

	if err != nil {
		log.Fatal(err)
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"code": http.StatusInternalServerError,
			"msg":  err.Error(),
		})
		ctx.Abort()
		return
	}

	authHeader := ctx.GetHeader("Authorization")
	_, cookieErr := ctx.Cookie("refreshToken")

	authHeaderSet, refreshCookieSet := false, false
	if authHeader != "" {
		authHeaderSet = true
	}

	if cookieErr == nil {
		refreshCookieSet = true
	}

	//If no access or refresh token is present error
	if !authHeaderSet && !refreshCookieSet {
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"code": http.StatusUnauthorized,
			"msg":  "Please provide an access token",
		})
		ctx.Abort()
		return
	}

	if authHeaderSet {

		jwtToken := strings.Replace(authHeader, "Bearer", "", -1)
		jwtToken = strings.TrimSpace(jwtToken)

		token, _, _ := validateJWT(jwtToken, pkParsed)

		err := token.Valid()

		if err != nil {
			authHeaderSet = false
		}
	}

	if !authHeaderSet && refreshCookieSet {

		//Request new access token from refresh token
		client := &http.Client{}

		url := authServerConfig.Auth.Server + authServerConfig.Auth.RefreshPath

		request, err := http.NewRequest(http.MethodGet, url, nil)

		cookie, _ := ctx.Request.Cookie("refreshToken")
		request.AddCookie(cookie)

		resp, err := client.Do(request)

		if resp != nil {
			defer resp.Body.Close()
		}

		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"code": http.StatusInternalServerError,
				"msg":  "Could not get a new access token",
			})
			ctx.Abort()
			return
		}

		bodyBytes, err := ioutil.ReadAll(resp.Body)

		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"code": http.StatusInternalServerError,
				"msg":  "Could not get a new access token",
			})
			ctx.Abort()
			return
		}

		var responseData authServerResponse
		err = json.Unmarshal(bodyBytes, &responseData)

		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"code": http.StatusInternalServerError,
				"msg":  "Could not get a new access token",
			})
			ctx.Abort()
			return
		}

		newtoken := responseData.Token
		authHeader = newtoken

		ctx.Writer.Header().Add("newAccessToken", newtoken)
	}

	//extract jwt token from header
	jwtToken := strings.Replace(authHeader, "Bearer", "", -1)
	jwtToken = strings.TrimSpace(jwtToken)

	//verify access token with public key
	token, isTokenValid, _ := validateJWT(jwtToken, pkParsed)

	if isTokenValid {
		req.Header.Add("x-auth-filter", "true")
		req.Header.Add("x-auth-roles", token.Roles)
		req.Header.Add("x-auth-user", token.Id)
	} else {
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"code": http.StatusUnauthorized,
			"msg":  "Your access token is invalid",
		})
		ctx.Abort()
		return
	}
}

func validateJWT(token string, publicKey *rsa.PublicKey) (*jwtTokenClaims, bool, error) {

	claims := jwtTokenClaims{}

	tok, err := jwt.ParseWithClaims(token, &claims, func(jwtToken *jwt.Token) (interface{}, error) {
		if _, ok := jwtToken.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected method: %s", jwtToken.Header["alg"])
		}

		return publicKey, nil
	})

	if err != nil {
		return &claims, false, fmt.Errorf("validate: %w", err)
	}

	if !tok.Valid {
		return &claims, false, fmt.Errorf("validate: invalid")
	}

	return &claims, true, nil
}

func authConfigParser(fileName string, authServerConfig *AuthServerConfig) {
	configFile, err := ioutil.ReadFile(fileName)

	if err != nil {
		panic(err)
	}

	err = yaml.Unmarshal(configFile, authServerConfig)

	if err != nil {
		panic(err)
	}
}
