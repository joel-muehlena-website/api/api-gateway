package userFilters

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	urlPkg "net/url"
	"strings"

	"github.com/gin-gonic/gin"
)

func RoleFilter(ctx *gin.Context, req *http.Request, args []interface{}) {

	type authServerResponse struct {
		Code int    `json:"status"`
		Msg  string `json:"msg"`
	}

	roleHeader := req.Header.Get("x-auth-roles")

	if roleHeader != "" {

		var argPermValue string = ""

		if len(args) > 0 {
			parsedValue, ok := args[0].(map[interface{}]interface{})
			if ok {
				if parsedValue["permissionValue"] != "" {
					argPermValue, ok = parsedValue["permissionValue"].(string)

					if !ok {
						argPermValue = ""
					}
				}
			}
		}

		var authServerConfig AuthServerConfig
		authConfigParser("config/config.yaml", &authServerConfig)

		url := authServerConfig.Auth.Server + authServerConfig.Auth.RoleVerifyPath
		path := strings.Split(ctx.FullPath(), "/")
		var permission string

		if argPermValue == "" {
			if req.Header.Get("x-api-scope") != "" {
				permission = req.Header.Get("x-api-scope") + ":" + path[3] + ":" + strings.ToLower(ctx.Request.Method)
			} else {
				permission = path[2] + ":" + strings.ToLower(ctx.Request.Method)
			}
		} else {
			permission = argPermValue
		}

		//query := "?permission=" + permission + "&roleName=" + roleHeader

		urlFormData := urlPkg.Values{}
		urlFormData.Set("roleName", roleHeader)
		urlFormData.Set("permission", permission)

		resp, err := http.Post(url, "application/x-www-form-urlencoded", strings.NewReader(urlFormData.Encode()))

		if err != nil {
			ctx.JSON(http.StatusUnauthorized, gin.H{
				"code": http.StatusUnauthorized,
				"msg":  "Please provide a valid role to access this resource",
			})
			ctx.Abort()
			return
		}

		defer resp.Body.Close()

		if resp.StatusCode == http.StatusOK {
			println("[Role] role is valid")
			req.Header.Add("x-auth-roleValid", "true")
		} else {
			println("[Role] role is invalid")
			println(resp.Status)
			bodyBytes, _ := ioutil.ReadAll(resp.Body)

			var responseData authServerResponse
			err = json.Unmarshal(bodyBytes, &responseData)

			ctx.JSON(resp.StatusCode, gin.H{
				"code": resp.StatusCode,
				"msg":  responseData.Msg,
			})
			ctx.Abort()
			return
		}

	} else {
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"code": http.StatusUnauthorized,
			"msg":  "Please provide a role to access this resource",
		})
		ctx.Abort()
		return
	}
}
